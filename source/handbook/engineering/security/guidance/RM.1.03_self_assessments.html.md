---
layout: markdown_page
title: "RM.1.03 - Self-Assessments (Not Applicable)"
---
 
## On this page
{:.no_toc}
 
- TOC
{:toc}
 
# RM.1.03 - Self-Assessments (Not Applicable)
 
## Control Statement

Quarterly, reviews shall be performed with approved documented specification to confirm personnel are following security policies and operational procedures pertaining to:
* log reviews quarterly
* firewall rule-set reviews
* applying configuration standards to new systems
* responding to security alerts
* change management processes
 
## Context

Reviews ensure that the process we've designed are operating as intended. The above list represents the common ways attackers can exploit systems to steal, delete, or alter information.
 
## Scope

**This control has been found to be not applicable (N/A) to GitLab.** The underlying control mappings apply only to PCI Service Providers. GitLab is not a PCI Service Provider, so this control is not applicable.
 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Self-Assessments issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/868) . 
 
## Framework Mapping
* PCI DSS V3.2.1
   * 12.11
   * 12.11.1
